package com.cryptobroker.cryptobroker

import retrofit2.http.GET
import retrofit2.http.Headers

/**
 * Api
 *
 * @author Maxim Berezin
 */
interface Api {


    @Headers("'X-CMC_PRO_API_KEY': '97a6afc0-0662-4e2b-8563-1afe7339c90a'")
    @GET("v1/cryptocurrency/listings/latest")
    fun getCryptoCurrencyListLatest()
}