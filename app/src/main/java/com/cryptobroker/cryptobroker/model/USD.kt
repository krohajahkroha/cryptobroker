package com.cryptobroker.cryptobroker.model

import com.google.gson.annotations.SerializedName

data class USD(

        @field:SerializedName("percent_change_1h")
        val percentChange1h: Double? = null,

        @field:SerializedName("last_updated")
        val lastUpdated: String? = null,

        @field:SerializedName("percent_change_24h")
        val percentChange24h: Double? = null,

        @field:SerializedName("market_cap")
        val marketCap: Long? = null,

        @field:SerializedName("price")
        val price: Double? = null,

        @field:SerializedName("volume_24h")
        val volume24h: Long? = null,

        @field:SerializedName("percent_change_7d")
        val percentChange7d: Double? = null
)