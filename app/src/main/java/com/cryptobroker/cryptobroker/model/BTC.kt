package com.cryptobroker.cryptobroker.model

import com.google.gson.annotations.SerializedName

data class BTC(

        @field:SerializedName("percent_change_1h")
        val percentChange1h: Int? = null,

        @field:SerializedName("last_updated")
        val lastUpdated: String? = null,

        @field:SerializedName("percent_change_24h")
        val percentChange24h: Int? = null,

        @field:SerializedName("market_cap")
        val marketCap: Int? = null,

        @field:SerializedName("price")
        val price: Int? = null,

        @field:SerializedName("volume_24h")
        val volume24h: Int? = null,

        @field:SerializedName("percent_change_7d")
        val percentChange7d: Int? = null
)