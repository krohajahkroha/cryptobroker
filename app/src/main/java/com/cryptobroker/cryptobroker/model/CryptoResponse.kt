package com.cryptobroker.cryptobroker.model

import com.google.gson.annotations.SerializedName

data class CryptoResponse(

        @field:SerializedName("data")
        val data: List<DataItem?>? = null,

        @field:SerializedName("status")
        val status: Status? = null
)