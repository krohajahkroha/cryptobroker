package com.cryptobroker.cryptobroker.model

import com.google.gson.annotations.SerializedName

data class Quotes(

        @field:SerializedName("BTC")
        val bTC: BTC? = null,

        @field:SerializedName("USD")
        val uSD: USD? = null
)