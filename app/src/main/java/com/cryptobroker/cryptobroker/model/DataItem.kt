package com.cryptobroker.cryptobroker.model

import com.google.gson.annotations.SerializedName

data class DataItem(

        @field:SerializedName("symbol")
        val symbol: String? = null,

        @field:SerializedName("circulating_supply")
        val circulatingSupply: Int? = null,

        @field:SerializedName("last_updated")
        val lastUpdated: String? = null,

        @field:SerializedName("total_supply")
        val totalSupply: Int? = null,

        @field:SerializedName("cmc_rank")
        val cmcRank: Int? = null,

        @field:SerializedName("platform")
        val platform: Any? = null,

        @field:SerializedName("tags")
        val tags: List<String?>? = null,

        @field:SerializedName("date_added")
        val dateAdded: String? = null,

        @field:SerializedName("quote")
        val quotes: Quotes? = null,

        @field:SerializedName("num_market_pairs")
        val numMarketPairs: Int? = null,

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("max_supply")
        val maxSupply: Int? = null,

        @field:SerializedName("id")
        val id: Int? = null,

        @field:SerializedName("slug")
        val slug: String? = null
)