package com.cryptobroker.cryptobroker

import com.google.gson.Gson
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * * Билдер инстанса Retrofit.
 *
 * @author Maxim Berezin
 */
class RetrofitBuilder
constructor(
        /**
         * Конфигурация API.
         */
        private val bksApiConfig: ApiConfig,
        /**
         * Билдер http-клиента
         */
        private val apiHttpClientBuilder: HttpClientBuilder,
        /**
         * Билдер GSON
         */
        private val gson: Gson
) {

    /**
     * Создает клиент ретрофит
     */
    fun build(): Retrofit {
        val client = apiHttpClientBuilder.build(BuildConfig.DEBUG)
        return Retrofit.Builder()
                .baseUrl(bksApiConfig.url)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }
}
