package com.cryptobroker.cryptobroker

/**
 * Конфигурация АПИ.
 *
 *
 * @author Maxim Berezin
 */
class ApiConfig(
        /**
         * API URL
         */
        val url: String)
