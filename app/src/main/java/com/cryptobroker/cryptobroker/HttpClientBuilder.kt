package com.cryptobroker.cryptobroker

import com.itkacher.okhttpprofiler.OkHttpProfilerInterceptor
import okhttp3.Cache
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit


/**
 * Билдер клиента OkHttp.
 *
 * @author Maxim Berezin
 */
class HttpClientBuilder
constructor(
        /**
         * Http-клиент.
         */
        private val baseOkHttpClient: OkHttpClient,
        private val cache: Cache) {

    fun build(isDebug: Boolean): OkHttpClient {
        val builder = baseOkHttpClient.newBuilder()
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
                .cache(cache)

        if (isDebug) {
            val okhttpProfiler = OkHttpProfilerInterceptor()
            builder.addInterceptor(okhttpProfiler)
        }

        return builder.build()
    }

    companion object {

        /**
         * Таймаут на подключение/чтение/запись (в секундах)
         */
        private const val TIMEOUT: Long = 30
    }
}
